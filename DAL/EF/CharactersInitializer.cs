﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Microsoft.EntityFrameworkCore;

namespace DAL.EF
{
    public static class CharactersInitializer
    {
        private static bool _hasbeenInitialized = false;

        public static void Initialize(CharactersDbContext context,
            bool dropCreateDatabase = true) 
        {
            if (!_hasbeenInitialized)
            {
                if (dropCreateDatabase)
                {
                    context.Database.EnsureDeleted();
                }

                if (context.Database.EnsureCreated())
                {
                    Seed(context);
                    _hasbeenInitialized = true;
                }
            }
        }

        private static void Seed(CharactersDbContext context)
        {
            Character char1 = new Character()
            {
                CharId = 1,
                Name = "Jason"
            };

            Character char2 = new Character()
            {
                CharId = 2,
                Name = "Anna"
            };

            Character char3 = new Character()
            {
                CharId = 3,
                Name = "John"
            };

            Background background1 = new Background()
            {
                BackId = 1,
                Name = "Noble",
                Characters = new List<CharacterBackground>(){}
            };

            Background background2 = new Background()
            {
                BackId = 2,
                Name = "Blacksmith",
                Characters = new List<CharacterBackground>(){}
            };

            CharacterBackground characterBackground1 = new CharacterBackground()
            {
                Character = char1,
                Background = background1
            };
            
            
            CharacterBackground characterBackground2 = new CharacterBackground()
            {
                Character = char2,
                Background = background2
            };
            
            
            CharacterBackground characterBackground3 = new CharacterBackground()
            {
                Character = char3,
                Background = background2
            };
            
            
            context.Add(char1);
            context.Add(char2);
            context.Add(char3);
            context.Add(background1);
            context.Add(background2);
            context.Add(characterBackground1);
            context.Add(characterBackground2);
            context.Add(characterBackground3);

            context.SaveChanges();
            foreach (var entry in context.ChangeTracker.Entries().ToList())
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}