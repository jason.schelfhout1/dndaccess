﻿using System.Linq;
using Domain;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;

namespace DAL.EF
{
    public class Repository : IRepository
    {
        private readonly CharactersDbContext _context;

        public Repository(CharactersDbContext context)
        {
            _context = context;
        }
        
        //===========================READ================================

        public Character ReadCharacter(int id)
        {
            return _context.Characters.Find(id);
        }

        public Background ReadBackground(int id)
        {
            return _context.Backgrounds.Find(id);
        }

        public AlliesOrganizations ReadAlliesOrganizations(int id)
        {
            return _context.AlliesOrganizations.Find(id);
        }

        public CharacterBackground ReadCharacterBackground(int id)
        {
            return _context.CharacterBackgrounds.Find(id);
        }

        public Appearance ReadAppearance(int id)
        {
            return _context.Appearances.Find(id);
        }

        public CharacterAllies ReadCharacterAllies(int id)
        {
            return _context.CharacterAllies.Find(id);
        }

        public Proficiency ReadProficiency(int id)
        {
            return _context.Proficiencies.Find(id);
        }

        public AttackSpellCasting ReadAttackSpellCasting(int id)
        {
            return _context.AttackSpellCastings.Find(id);
        }

        public CombatStats ReadCombatStats(int id)
        {
            return _context.CombatStats.Find(id);
        }

        public HitDie ReadHitDie(int id)
        {
            return _context.HitDice.Find(id);
        }

        public Equipment ReadEquipment(int id)
        {
            return _context.Equipments.Find(id);
        }

        public Item ReadItem(int id)
        {
            return _context.Items.Find(id);
        }

        public Treasure ReadTreasure(int id)
        {
            return _context.Treasures.Find(id);
        }

        public Weapon ReadWeapon(int id)
        {
            return _context.Weapons.Find(id);
        }

        public Spell ReadSpell(int id)
        {
            return _context.Spells.Find(id);
        }

        public Spellcast ReadSpellcast(int id)
        {
            return _context.Spellcasts.Find(id);
        }

        public SpellList ReadSpellList(int id)
        {
            return _context.SpellLists.Find(id);
        }

        public AdditionalStats ReadAdditionalStats(int id)
        {
            return _context.AdditionalStats.Find(id);
        }

        public Attribute ReadAttribute(int id)
        {
            return _context.Attributes.Find(id);
        }

        public Stats ReadStats(int id)
        {
            return _context.Stats.Find(id);
        }

        public Skills ReadSkills(int id)
        {
            return _context.Skills.Find(id);
        }

        //===========================READALL================================

        public IQueryable<Character> ReadAllCharacters()
        {
            return _context.Characters;
        }

        public IQueryable<Background> ReadAllBackgrounds()
        {
            return _context.Backgrounds;
        }

        public IQueryable<AlliesOrganizations> ReadAllAlliesOrganizations()
        {
            return _context.AlliesOrganizations;
        }

        public IQueryable<CharacterBackground> ReadAllCharacterBackground()
        {
            return _context.CharacterBackgrounds;
        }

        public IQueryable<Appearance> ReadAllAppearances()
        {
            return _context.Appearances;
        }

        public IQueryable<CharacterAllies> ReadAllCharacterAllies()
        {
            return _context.CharacterAllies;
        }

        public IQueryable<Proficiency> ReadAllProficiencies()
        {
            return _context.Proficiencies;
        }

        public IQueryable<AttackSpellCasting> ReadAllAttackSpellCastings()
        {
            return _context.AttackSpellCastings;
        }

        public IQueryable<CombatStats> ReadAllCombatStats()
        {
            return _context.CombatStats;
        }

        public IQueryable<HitDie> ReadAllHitDice()
        {
            return _context.HitDice;
        }

        public IQueryable<Equipment> ReadAllEquipments()
        {
            return _context.Equipments;
        }

        public IQueryable<Item> ReadAllItems()
        {
            return _context.Items;
        }

        public IQueryable<Treasure> ReadAllTreasures()
        {
            return _context.Treasures;
        }

        public IQueryable<Weapon> ReadAllWeapons()
        {
            return _context.Weapons;
        }

        public IQueryable<Spell> ReadAllSpells()
        {
            return _context.Spells;
        }

        public IQueryable<Spellcast> ReadAllSpellcasts()
        {
            return _context.Spellcasts;
        }

        public IQueryable<SpellList> ReadAllSpellLists()
        {
            return _context.SpellLists;
        }

        public IQueryable<AdditionalStats> ReadAllAdditionalStats()
        {
            return _context.AdditionalStats;
        }

        public IQueryable<Attribute> ReadAllAttributes()
        {
            return _context.Attributes;
        }

        public IQueryable<Skills> ReadAllSkills()
        {
            return _context.Skills;
        }

        public IQueryable<Stats> ReadAllStats()
        {
            return _context.Stats;
        }

        //===========================CREATE================================

        public void CreateCharacter(Character character)
        {
            _context.Characters.Add(character);
            _context.SaveChanges();
        }

        public void CreateCombatStats(CombatStats combatStats)
        {
            _context.CombatStats.Add(combatStats);
            _context.SaveChanges();
        }

        public void CreateHitDie(HitDie hitDie)
        {
            _context.HitDice.Add(hitDie);
            _context.SaveChanges();
        }

        public void CreateEquipment(Equipment equipment)
        {
            _context.Equipments.Add(equipment);
            _context.SaveChanges();
        }

        public void CreateAppearance(Appearance appearance)
        {
            _context.Appearances.Add(appearance);
            _context.SaveChanges();
        }

        public void CreateSpellcast(Spellcast spellcast)
        {
            _context.Spellcasts.Add(spellcast);
            _context.SaveChanges();
        }

        public void CreateStats(Stats stats)
        {
            _context.Stats.Add(stats);
            _context.SaveChanges();
        }

        public void CreateBackground(Background background)
        {
            _context.Backgrounds.Add(background);
            _context.SaveChanges();
        }

        public void CreateAlliesOrganizations(AlliesOrganizations alliesOrganizations)
        {
            _context.AlliesOrganizations.Add(alliesOrganizations);
            _context.SaveChanges();
        }

        public void CreateProficiencies(Proficiency proficiency)
        {
            _context.Proficiencies.Add(proficiency);
            _context.SaveChanges();
        }

        public void CreateAttackSpellCastings(AttackSpellCasting attackSpellCasting)
        {
            _context.AttackSpellCastings.Add(attackSpellCasting);
            _context.SaveChanges();
        }

        public void CreateItems(Item item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
        }

        public void CreateTreasures(Treasure treasure)
        {
            _context.Treasures.Add(treasure);
            _context.SaveChanges();
        }

        public void CreateWeapons(Weapon weapon)
        {
            _context.Weapons.Add(weapon);
            _context.SaveChanges();
        }

        public void CreateSpells(Spell spell)
        {
            _context.Spells.Add(spell);
            _context.SaveChanges();
        }

        public void CreateSpellLists(SpellList spellList)
        {
            _context.SpellLists.Add(spellList);
            _context.SaveChanges();
        }

        public void CreateAdditionalStats(AdditionalStats additionalStats)
        {
            _context.AdditionalStats.Add(additionalStats);
            _context.SaveChanges();
        }

        public void CreateAttributes(Attribute attribute)
        {
            _context.Attributes.Add(attribute);
            _context.SaveChanges();
        }

        public void CreateSkills(Skills skills)
        {
            _context.Skills.Add(skills);
            _context.SaveChanges();
        }

        //===========================DELETE================================

        public void DeleteCharacter(int id)
        {
            Character c = ReadCharacter(id);
            _context.Characters.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteBackground(int id)
        {
            Background c = ReadBackground(id);
            _context.Backgrounds.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteCharacterBackground(int charId, int backId)
        {
            var c = _context.CharacterBackgrounds.Single(
                a => a.Character.CharId == charId 
                     && a.Background.BackId == backId);
            _context.CharacterBackgrounds.Remove(c);
            _context.SaveChanges();
        }
        
        public void DeleteCharacterAllies(int charId, int alliesId)
        {
            var c = _context.CharacterAllies.Single(
                a => a.Character.CharId == charId 
                     && a.AlliesOrganizations.Id == alliesId);
            _context.CharacterAllies.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteAlliesOrganizations(int id)
        {
            AlliesOrganizations c = ReadAlliesOrganizations(id);
            _context.AlliesOrganizations.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteAppearance(int id)
        {
            Appearance c = ReadAppearance(id);
            _context.Appearances.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteProficiency(int id)
        {
            Proficiency c = ReadProficiency(id);
            _context.Proficiencies.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteAttackSpellCasting(int id)
        {
            AttackSpellCasting c = ReadAttackSpellCasting(id);
            _context.AttackSpellCastings.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteCombatStats(int id)
        {
            CombatStats c = ReadCombatStats(id);
            _context.CombatStats.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteHitDie(int id)
        {
            HitDie c = ReadHitDie(id);
            _context.HitDice.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteEquipment(int id)
        {
            Equipment c = ReadEquipment(id);
            _context.Equipments.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteItem(int id)
        {
            Item c = ReadItem(id);
            _context.Items.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteTreasure(int id)
        {
            Treasure c = ReadTreasure(id);
            _context.Treasures.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteWeapon(int id)
        {
            Weapon c = ReadWeapon(id);
            _context.Weapons.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteSpell(int id)
        {
            Spell c = ReadSpell(id);
            _context.Spells.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteSpellcast(int id)
        {
            Spellcast c = ReadSpellcast(id);
            _context.Spellcasts.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteSpellList(int id)
        {
            SpellList c = ReadSpellList(id);
            _context.SpellLists.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteAdditionalStats(int id)
        {
            AdditionalStats c = ReadAdditionalStats(id);
            _context.AdditionalStats.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteAttribute(int id)
        {
            Attribute c = ReadAttribute(id);
            _context.Attributes.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteSkills(int id)
        {
            Skills c = ReadSkills(id);
            _context.Skills.Remove(c);
            _context.SaveChanges();
        }

        public void DeleteStats(int id)
        {
            Stats c = ReadStats(id);
            _context.Stats.Remove(c);
            _context.SaveChanges();
        }

        //===========================LINK================================

        public void LinkCharacterToBackground(int charId, int backId)
        {
            Character c = ReadCharacter(charId);
            Background b = ReadBackground(backId);
            CharacterBackground cb = new CharacterBackground()
            {
                Character = c,
                Background = b
            };
            _context.CharacterBackgrounds.Add(cb);
            _context.SaveChanges();
        }

        public void LinkCharacterToAllies(int charId, int alliesId)
        {
            
            Character c = ReadCharacter(charId);
            AlliesOrganizations a = ReadAlliesOrganizations(alliesId);
            CharacterAllies ca = new CharacterAllies()
            {
                Character = c,
                AlliesOrganizations = a
            };
            _context.CharacterAllies.Add(ca);
            _context.SaveChanges();
        }
    }
}