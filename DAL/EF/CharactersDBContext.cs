﻿using Domain;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Attribute = Domain.StatsF.Attribute;

namespace DAL.EF
{
    public class CharactersDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        { 
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(
                        "Data Source=../../../../DAL/Characters.db")
                    .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddDebug()));
            }
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
             * 1-on-0..1
             */
            /*
             modelBuilder.Entity<Character>()
                .HasOptional(a => a.Appearance) 
                .WithRequired(b => b.Character);*/
            /*
             * 1-on-N
             */
            /*
             modelBuilder
                .Entity<Character>()
                .HasOne(a => a.Appearance)
                .WithMany(b => b.Character)
                .IsRequired();*/
            
            //Proficiencies
            modelBuilder
                .Entity<Weapon>()
                .HasOne(a => a.Proficiency)
                .WithMany(b => b.WeaponProficiencies);
            modelBuilder
                .Entity<Language>()
                .HasOne(a => a.Proficiency)
                .WithMany(b => b.Languages);
            modelBuilder
                .Entity<Armor>()
                .HasOne(a => a.Proficiency)
                .WithMany(b => b.ArmorProficiencies);
            modelBuilder
                .Entity<Tool>()
                .HasOne(a => a.Proficiency)
                .WithMany(b => b.ToolProficiencies);
            
            //Attributes
            modelBuilder
                .Entity<AdditionalAttribute>()
                .HasOne(a => a.AdditionalStats)
                .WithMany(b => b.SavingThrows);

            modelBuilder.Entity<Attribute>()
                .Property(a => a.Type)
                .HasConversion<string>();
            modelBuilder.Entity<AdditionalAttribute>()
                .Property(a => a.AttributeTypeEnum)
                .HasConversion<string>();
                
            /*
             * N-on-N
             */
             /*modelBuilder.Entity<Background>()
                 .HasMany(a => a.Characters);*/

             modelBuilder.Entity<Background>()
                 .HasMany(a => a.Characters);
             modelBuilder.Entity<CharacterBackground>()
                 .HasOne(a => a.Background)
                 .WithMany(s => s.Characters)
                 .HasForeignKey("BackgroundFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<CharacterBackground>()
                 .HasOne(a => a.Character)
                 .WithMany(s => s.Backgrounds)
                 .HasForeignKey("CharacterFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<CharacterBackground>()
                 .HasKey("BackgroundFK_shadow", "CharacterFK_shadow");
             
             modelBuilder.Entity<Person>()
                 .HasMany(a => a.AlliesOrganizations);
             modelBuilder.Entity<AlliesPerson>()
                 .HasOne(a => a.Person)
                 .WithMany(s => s.AlliesOrganizations)
                 .HasForeignKey("PersonFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<AlliesPerson>()
                 .HasOne(a => a.AlliesOrganizations)
                 .WithMany(s => s.Persons)
                 .HasForeignKey("AlliesOrganizationsFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<AlliesPerson>()
                 .HasKey("PersonFK_shadow", "AlliesOrganizationsFK_shadow");
             
             modelBuilder.Entity<AlliesOrganizations>()
                 .HasMany(a => a.Characters);
             modelBuilder.Entity<CharacterAllies>()
                 .HasOne(a => a.Character)
                 .WithMany(s => s.AlliesOrganizations)
                 .HasForeignKey("CharacterFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<CharacterAllies>()
                 .HasOne(a => a.AlliesOrganizations)
                 .WithMany(s => s.Characters)
                 .HasForeignKey("AlliesOrganizationsFK_shadow")
                 .IsRequired();
             modelBuilder.Entity<CharacterAllies>()
                 .HasKey("CharacterFK_shadow", "AlliesOrganizationsFK_shadow");
             
            
            /*modelBuilder
                .Entity<CharacterBackground>()
                .Property("CharacterId")
                .IsRequired();
            
            modelBuilder
                .Entity<CharacterBackground>()
                .Property("BackgroundId")
                .IsRequired();
            
            modelBuilder
                .Entity<CharacterBackground>()
                .HasKey("CharacterId", "BackgroundId");*/
        }

        public CharactersDbContext()
        {
            CharactersInitializer.Initialize(this, false);
        }

        public DbSet<Character> Characters { get; set; }

        public DbSet<Background> Backgrounds { get; set; }

        public DbSet<AlliesPerson> AlliesPersons { get; set; }

        public DbSet<CharacterBackground> CharacterBackgrounds { get; set; }

        public DbSet<AlliesOrganizations> AlliesOrganizations { get; set; }

        public DbSet<Appearance> Appearances { get; set; }

        public DbSet<CharacterAllies> CharacterAllies { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Proficiency> Proficiencies { get; set; }

        public DbSet<Tool> Tools { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<AttackSpellCasting> AttackSpellCastings { get; set; }

        public DbSet<CombatStats> CombatStats { get; set; }

        public DbSet<HitDie> HitDice { get; set; }

        public DbSet<Equipment> Equipments { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Treasure> Treasures { get; set; }

        public DbSet<Weapon> Weapons { get; set; }

        public DbSet<Armor> Armors { get; set; }

        public DbSet<Spell> Spells { get; set; }

        public DbSet<Spellcast> Spellcasts { get; set; }

        public DbSet<SpellList> SpellLists { get; set; }

        public DbSet<AdditionalAttribute> AdditionalAttributes { get; set; }

        public DbSet<AdditionalStats> AdditionalStats { get; set; }

        public DbSet<Attribute> Attributes { get; set; }

        public DbSet<Skills> Skills { get; set; }

        public DbSet<Stats> Stats { get; set; }
    }
}