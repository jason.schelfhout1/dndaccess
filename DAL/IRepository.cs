﻿using System.Linq;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;

namespace DAL
{
    public interface IRepository
    {
        
        //===========================READ================================
        Character ReadCharacter(int id);

        Background ReadBackground(int id);
        
        AlliesOrganizations ReadAlliesOrganizations(int id);
        
        CharacterBackground ReadCharacterBackground(int id);
        
        Appearance ReadAppearance(int id);
        
        CharacterAllies ReadCharacterAllies(int id);
        
        Proficiency ReadProficiency(int id);
        
        AttackSpellCasting ReadAttackSpellCasting(int id);
        
        CombatStats ReadCombatStats(int id);
        
        HitDie ReadHitDie(int id);
        
        Equipment ReadEquipment(int id);
        
        Item ReadItem(int id);
        
        Treasure ReadTreasure(int id);
        
        Weapon ReadWeapon(int id);
        
        Spell ReadSpell(int id);
        
        Spellcast ReadSpellcast(int id);
        
        SpellList ReadSpellList(int id);
        
        AdditionalStats ReadAdditionalStats(int id);
        
        Attribute ReadAttribute(int id);

        Stats ReadStats(int id);
        
        Skills ReadSkills(int id);
        
        //===========================READALL================================

        IQueryable<Character> ReadAllCharacters();

        IQueryable<Background> ReadAllBackgrounds();

        IQueryable<AlliesOrganizations> ReadAllAlliesOrganizations();

        IQueryable<CharacterBackground> ReadAllCharacterBackground();
        IQueryable<Appearance> ReadAllAppearances();

        IQueryable<CharacterAllies> ReadAllCharacterAllies();

        IQueryable<Proficiency> ReadAllProficiencies();

        IQueryable<AttackSpellCasting> ReadAllAttackSpellCastings();

        IQueryable<CombatStats> ReadAllCombatStats();

        IQueryable<HitDie> ReadAllHitDice();

        IQueryable<Equipment> ReadAllEquipments();

        IQueryable<Item> ReadAllItems();

        IQueryable<Treasure> ReadAllTreasures();

        IQueryable<Weapon> ReadAllWeapons();

        IQueryable<Spell> ReadAllSpells();

        IQueryable<Spellcast> ReadAllSpellcasts();

        IQueryable<SpellList> ReadAllSpellLists();

        IQueryable<AdditionalStats> ReadAllAdditionalStats();

        IQueryable<Attribute> ReadAllAttributes();

        IQueryable<Skills> ReadAllSkills();

        IQueryable<Stats> ReadAllStats();
        
        //===========================CREATE================================

        void CreateCharacter(Character character);

        void CreateCombatStats(CombatStats combatStats);

        void CreateHitDie(HitDie hitDie);

        void CreateBackground(Background background);

        void CreateEquipment(Equipment equipment);

        void CreateAppearance(Appearance appearance);

        void CreateSpellcast(Spellcast spellcast);

        void CreateStats(Stats stats);

        void CreateAlliesOrganizations(AlliesOrganizations alliesOrganizations);

        void CreateProficiencies(Proficiency proficiency);

        void CreateAttackSpellCastings(AttackSpellCasting attackSpellCasting);

        void CreateItems(Item item);

        void CreateTreasures(Treasure treasure);

        void CreateWeapons(Weapon weapon);

        void CreateSpells(Spell spell);

        void CreateSpellLists(SpellList spellList);

        void CreateAdditionalStats(AdditionalStats additionalStats);

        void CreateAttributes(Attribute attribute);

        void CreateSkills(Skills skills);
        
        //===========================DELETE================================

        void DeleteCharacter(int id);
        
        void DeleteBackground(int id);
        
        void DeleteCharacterBackground(int charId, int backId);
        
        void DeleteCharacterAllies(int charId, int alliesId);
        
        void DeleteAlliesOrganizations(int id);
        
        void DeleteAppearance(int id);
        
        void DeleteProficiency(int id);
        
        void DeleteAttackSpellCasting(int id);
        
        void DeleteCombatStats(int id);
        
        void DeleteHitDie(int id);
        
        void DeleteEquipment(int id);
        
        void DeleteItem(int id);
        
        void DeleteTreasure(int id);
        
        void DeleteWeapon(int id);
        
        void DeleteSpell(int id);
        
        void DeleteSpellcast(int id);
        
        void DeleteSpellList(int id);
        
        void DeleteAdditionalStats(int id);
        
        void DeleteAttribute(int id);
        
        void DeleteSkills(int id);
        
        void DeleteStats(int id);
        
        //===========================LINK================================

        void LinkCharacterToBackground(int charId, int backId);

        void LinkCharacterToAllies(int charId, int alliesId);
    }
}