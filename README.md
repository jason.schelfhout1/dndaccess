D&D project where you'll be able to create a D&D character or upload it into the database.
With the usage of MVC and N-Layer (as overhead).

Packages used:
Microsoft.EntityFrameworkCore
Microsoft.EntityFrameworkCore.Sqlite
Microsoft.Extensions.Logging.Debug
Swashbuckle.AspNetCore
Microsoft.AspNetCore.Razor
