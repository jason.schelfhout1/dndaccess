﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Global;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;

namespace DND.BL
{
    public class Manager : IManager
    {
        private readonly IRepository _repository;
        
        public Manager(IRepository repository)
        {
            _repository = repository;
        }


        public Character AddCharacter(int charId, string name, string surName, Gender gender, string charClass, int level, string race,
            string subrace, ICollection<CharacterBackground> backgrounds, Alignment alignment, Appearance appearance, string playerName,
            int expPoints, string backstory)
        {
            Character c = new Character
            {
                CharId = charId,
                Name = name,
                SurName = surName,
                Gender = gender,
                CharClass = charClass,
                Level = level,
                Race = race,
                Subrace = subrace,
                Backgrounds = backgrounds,
                Alignment = alignment,
                Appearance = appearance,
                PlayerName = playerName,
                EXPPoints = expPoints,
                Backstory = backstory
            };

            return c;
        }

        public AlliesOrganizations AddAlliesOrganizations(int id, string name, string description, ICollection<AlliesPerson> persons)
        {
            AlliesOrganizations a = new AlliesOrganizations
            {
                Id = id,
                Name = name,
                Description = description,
                Persons = persons
            };
            
            _repository.CreateAlliesOrganizations(a);
            return a;
        }

        public Appearance AddAppearance(int id, int age, int height, string eyes, string skin, string hair)
        {
            Appearance a = new Appearance
            {
                Id = id,
                Age = age,
                Height = height,
                Eyes = eyes,
                Skin = skin,
                Hair = hair
            };
            _repository.CreateAppearance(a);
            return a;
        }

        public Background AddBackground(int backId, string name, string description, ICollection<CharacterBackground> characters)
        {
            Background b = new Background
            {
                BackId = backId,
                Name = name,
                Description = description,
                Characters = characters
            };
            _repository.CreateBackground(b);
            return b;
        }

        public Proficiency AddProficiency(int id, ICollection<Tool> toolProficiencies, ICollection<Armor> armorProficiencies,
            ICollection<Weapon> weaponProficiencies, ICollection<Language> languages)
        {
            Proficiency p = new Proficiency
            {
                Id = id,
                ToolProficiencies = toolProficiencies,
                ArmorProficiencies = armorProficiencies,
                WeaponProficiencies = weaponProficiencies,
                Languages = languages
            };
            _repository.CreateProficiencies(p);
            return p;
        }

        public CombatStats AddCombatStats(int id, int armorClass, int initiative, int speed, int? currentHp, int maxHp, int? tempHp,
            HitDie hitDie, int deathSuccess, int deathFailures, AttackSpellCasting attackSpellCasting)
        {
            CombatStats c = new CombatStats
            {
                Id = id,
                ArmorClass = armorClass,
                Initiative = initiative,
                Speed = speed,
                CurrentHp = currentHp,
                MaxHp = maxHp,
                TempHp = tempHp,
                HitDie = hitDie,
                DeathSuccess = deathSuccess,
                DeathFailures = deathFailures,
                AttackSpellCasting = attackSpellCasting
            };
            _repository.CreateCombatStats(c);
            return c;
        }

        public AttackSpellCasting AddAttackSpellCasting(int id, ICollection<Spell> spells, ICollection<Weapon> weapons, int? numberOfAttacks)
        {
            AttackSpellCasting a = new AttackSpellCasting
            {
                Id = id,
                Spells = spells,
                Weapons = weapons,
                NumberOfAttacks = numberOfAttacks
            };
            _repository.CreateAttackSpellCastings(a);
            return a;
        }

        public HitDie AddHitDie(int id, int amount, int amountDice, Die die, int bonus, string type)
        {
            HitDie h = new HitDie
            {
                Id = id,
                Amount = amount,
                AmountDice = amountDice,
                Die = die,
                Bonus = bonus,
                Type = type
            };
            _repository.CreateHitDie(h);
            return h;
        }

        public Equipment AddEquipment(int id, ICollection<Item> items, Treasure treasure)
        {
            Equipment e = new Equipment
            {
                Id = id,
                Items = items,
                Treasure = treasure
            };
            _repository.CreateEquipment(e);
            return e;
        }

        public Item AddItem(int id, string name, int amount)
        {
            Item i = new Item
            {
                Id = id,
                Name = name,
                Amount = amount
            };
            _repository.CreateItems(i);
            return i;
        }

        public Treasure AddTreasure(int id, ICollection<Item> items, int copperPiece, int silverPiece, int electrumPiece, int goldPiece,
            int platimumPiece)
        {
            Treasure t = new Treasure
            {
                Id = id,
                Items = items,
                CopperPiece = copperPiece,
                SilverPiece = silverPiece,
                ElectrumPiece = electrumPiece,
                GoldPiece = goldPiece,
                PlatimumPiece = platimumPiece
            };
            _repository.CreateTreasures(t);
            return t;
        }

        public Weapon AddWeapon(int id, string name, int amount, HitDie hitDie, string profRequirement)
        {
            Item w = new Weapon
            {
                Name = name,
                Amount = amount,
                Id = id,
                HitDie = hitDie,
                ProfRequirement = profRequirement
            };
            _repository.CreateWeapons((Weapon) w);
            return (Weapon) w;
        }

        public Spell AddSpell(int id, int level, string name, string description, string castingTime, string range, string attackSave,
            string type)
        {
            Spell s = new Spell
            {
                Id = id,
                Level = level,
                Name = name,
                Description = description,
                CastingTime = castingTime,
                Range = range,
                AttackSave = attackSave,
                Type = type
            };
            _repository.CreateSpells(s);
            return s;
        }

        public Spellcast AddSpellcast(int id, string spellcastingAbility, int spellSaveDc, int spellAtkBonus, ICollection<SpellList> spellLists)
        {
            Spellcast s = new Spellcast
            {
                Id = id,
                SpellcastingAbility = spellcastingAbility,
                SpellSaveDC = spellSaveDc,
                SpellATKBonus = spellAtkBonus,
                SpellLists = spellLists
            };
            _repository.CreateSpellcast(s);
            return s;
        }

        public SpellList AddSpellList(int id, bool cantrip, int level, int spellSlot, int slotExpended, ICollection<Spell> spells)
        {
            SpellList s = new SpellList
            {
                Id = id,
                Cantrip = cantrip,
                Level = level,
                SpellSlot = spellSlot,
                SlotExpended = slotExpended,
                Spells = spells
            };
            _repository.CreateSpellLists(s);
            return s;
        }

        public Stats AddStats(int id, Attribute attribute, Skills skills, AdditionalStats additionalStats)
        {
            Stats s = new Stats
            {
                Id = id,
                Attribute = attribute,
                Skills = skills,
                AdditionalStats = additionalStats
            };
            _repository.CreateStats(s);
            return s;
        }

        public AdditionalStats AddAdditionalStats(int id, ICollection<AdditionalAttribute> savingThrows, int proficiencyBonus, int? inspiration,
            int? passiveWisdom)
        {
            AdditionalStats a = new AdditionalStats
            {
                Id = id,
                SavingThrows = savingThrows,
                ProficiencyBonus = proficiencyBonus,
                Inspiration = inspiration,
                PassiveWisdom = passiveWisdom
            };
            _repository.CreateAdditionalStats(a);
            return a;
        }

        public Attribute AddAttribute(int id, string name, AttributeTypeEnum type, int points)
        {
            Attribute a = new Attribute
            {
                Id = id,
                Name = name,
                Type = type,
                Points = points
            };
            _repository.CreateAttributes(a);
            return a;
        }

        public Skills AddSkills(int id, int acrobatics, int animalHandling, int arcana, int athletics, int deception, int history,
            int insight, int intimidation, int investigation, int medicine, int nature, int perception, int performance,
            int persuasion, int religion, int sleightOfHand, int stealth, int survival)
        {
            Skills s = new Skills
            {
                Id = id,
                Acrobatics = acrobatics,
                AnimalHandling = animalHandling,
                Arcana = arcana,
                Athletics = athletics,
                Deception = deception,
                History = history,
                Insight = insight,
                Intimidation = intimidation,
                Investigation = investigation,
                Medicine = medicine,
                Nature = nature,
                Perception = perception,
                Performance = performance,
                Persuasion = persuasion,
                Religion = religion,
                SleightOfHand = sleightOfHand,
                Stealth = stealth,
                Survival = survival
            };
            _repository.CreateSkills(s);
            return s;
        }

        public Character GetCharacter(int charId)
        {
            return _repository.ReadCharacter(charId);
        }

        public AlliesOrganizations GetAlliesOrganizations(int id)
        {
            return _repository.ReadAlliesOrganizations(id);
        }

        public Appearance GetAppearance(int id)
        {
            return _repository.ReadAppearance(id);
        }

        public Background GetBackground(int backId)
        {
            return _repository.ReadBackground(backId);
        }

        public Proficiency GetProficiency(int id)
        {
            return _repository.ReadProficiency(id);
        }

        public CombatStats GetCombatStats(int id)
        {
            return _repository.ReadCombatStats(id);
        }

        public AttackSpellCasting GetAttackSpellCasting(int id)
        {
            return _repository.ReadAttackSpellCasting(id);
        }

        public HitDie GetHitDie(int id)
        {
            return _repository.ReadHitDie(id);
        }

        public Equipment GetEquipment(int id)
        {
            return _repository.ReadEquipment(id);
        }

        public Item GetItem(int id)
        {
            return _repository.ReadItem(id);
        }

        public Treasure GetTreasure(int id)
        {
            return _repository.ReadTreasure(id);
        }

        public Weapon GetWeapon(int id)
        {
            return _repository.ReadWeapon(id);
        }

        public Spell GetSpell(int id)
        {
            return _repository.ReadSpell(id);
        }

        public Spellcast GetSpellcast(int id)
        {
            return _repository.ReadSpellcast(id);
        }

        public SpellList GetSpellList(int id)
        {
            return _repository.ReadSpellList(id);
        }

        public Stats GetStats(int id)
        {
            return _repository.ReadStats(id);
        }

        public AdditionalStats GetAdditionalStats(int id)
        {
            return _repository.ReadAdditionalStats(id);
        }

        public Attribute GetAttribute(int id)
        {
            return _repository.ReadAttribute(id);
        }

        public Skills GetSkills(int id)
        {
            return _repository.ReadSkills(id);
        }

        public IQueryable<Character> GetAllCharacters()
        {
            return _repository.ReadAllCharacters();
        }

        public IQueryable<Background> GetAllBackgrounds()
        {
            return _repository.ReadAllBackgrounds();
        }

        public IQueryable<AlliesOrganizations> GetAllAlliesOrganizations()
        {
            return _repository.ReadAllAlliesOrganizations();
        }

        public IQueryable<CharacterBackground> GetAllCharacterBackgrounds()
        {
            return _repository.ReadAllCharacterBackground();
        }

        public IQueryable<Appearance> GetAllAppearances()
        {
            return _repository.ReadAllAppearances();
        }

        public IQueryable<CharacterAllies> GetAllCharacterAllies()
        {
            return _repository.ReadAllCharacterAllies();
        }

        public IQueryable<Proficiency> GetAllProficiencies()
        {
            return _repository.ReadAllProficiencies();
        }

        public IQueryable<AttackSpellCasting> GetAllAttackSpellCastings()
        {
            return _repository.ReadAllAttackSpellCastings();
        }

        public IQueryable<CombatStats> GetAllCombatStats()
        {
            return _repository.ReadAllCombatStats();
        }

        public IQueryable<HitDie> GetAllHitDice()
        {
            return _repository.ReadAllHitDice();
        }

        public IQueryable<Equipment> GetAllEquipments()
        {
            return _repository.ReadAllEquipments();
        }

        public IQueryable<Item> GetAllItems()
        {
            return _repository.ReadAllItems();
        }

        public IQueryable<Treasure> GetAllTreasures()
        {
            return _repository.ReadAllTreasures();
        }

        public IQueryable<Weapon> GetAllWeapons()
        {
            return _repository.ReadAllWeapons();
        }

        public IQueryable<Spell> GetAllSpells()
        {
            return _repository.ReadAllSpells();
        }

        public IQueryable<Spellcast> GetAllSpellcasts()
        {
            return _repository.ReadAllSpellcasts();
        }

        public IQueryable<SpellList> GetAllSpellLists()
        {
            return _repository.ReadAllSpellLists();
        }

        public IQueryable<AdditionalStats> GetAllAdditionalStats()
        {
            return _repository.ReadAllAdditionalStats();
        }

        public IQueryable<Attribute> GetAllAttributes()
        {
            return _repository.ReadAllAttributes();
        }

        public IQueryable<Skills> GetAllSkills()
        {
            return _repository.ReadAllSkills();
        }

        public IQueryable<Stats> GetAllStats()
        {
            return _repository.ReadAllStats();
        }

        public void RemoveCharacter(int id)
        {
            _repository.DeleteCharacter(id);
        }

        public void RemoveBackground(int id)
        {
            _repository.DeleteBackground(id);
        }

        public void RemoveAlliesOrganizations(int id)
        {
            _repository.DeleteAlliesOrganizations(id);
        }

        public void RemoveCharacterBackground(int charId, int backId)
        {
            _repository.DeleteCharacterBackground(charId, backId);
        }

        public void RemoveCharacterAllies(int charId, int alliesId)
        {
            _repository.DeleteCharacterAllies(charId, alliesId);
        }

        public void RemoveAppearance(int id)
        {
            _repository.DeleteAppearance(id);
        }

        public void RemoveProficiency(int id)
        {
            _repository.DeleteProficiency(id);
        }

        public void RemoveAttackSpellCasting(int id)
        {
            _repository.DeleteAttackSpellCasting(id);
        }

        public void RemoveCombatStats(int id)
        {
            _repository.DeleteCombatStats(id);
        }

        public void RemoveHitDie(int id)
        {
            _repository.DeleteHitDie(id);
        }

        public void RemoveEquipment(int id)
        {
            _repository.DeleteEquipment(id);
        }

        public void RemoveItem(int id)
        {
            _repository.DeleteItem(id);
        }

        public void RemoveTreasure(int id)
        {
            _repository.DeleteTreasure(id);
        }

        public void RemoveWeapon(int id)
        {
            _repository.DeleteWeapon(id);
        }

        public void RemoveSpell(int id)
        {
            _repository.DeleteSpell(id);
        }

        public void RemoveSpellcast(int id)
        {
            _repository.DeleteSpellcast(id);
        }

        public void RemoveSpellList(int id)
        {
            _repository.DeleteSpellList(id);
        }

        public void RemoveAdditionalStats(int id)
        {
            _repository.DeleteAdditionalStats(id);
        }

        public void RemoveAttribute(int id)
        {
            _repository.DeleteAttribute(id);
        }

        public void RemoveSkills(int id)
        {
            _repository.DeleteSkills(id);
        }

        public void RemoveStats(int id)
        {
            _repository.DeleteStats(id);
        }

        public void LinkCharacterToBackground(int charId, int backId)
        {
            _repository.LinkCharacterToBackground(charId, backId);
        }

        public void LinkCharacterToAllies(int charId, int alliesId)
        {
            _repository.LinkCharacterToAllies(charId, alliesId);
        }
    }
}