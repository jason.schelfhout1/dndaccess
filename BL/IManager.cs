﻿using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Global;
using Domain.Main;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;
using Attribute = Domain.StatsF.Attribute;

namespace DND.BL
{
    public interface IManager
    {
        
        //===========================================ADD===========================================
        //Main
        Character AddCharacter(int charId, string name, string surName, Gender gender, string charClass, int level,
            string race, string subrace, ICollection<CharacterBackground> backgrounds, Alignment alignment,
            Appearance appearance, string playerName, int expPoints, string backstory);

        AlliesOrganizations AddAlliesOrganizations(int id, string name, string description,
            ICollection<AlliesPerson> persons);

        Appearance AddAppearance(int id, int age, int height, string eyes, string skin, string hair);

        Background AddBackground(int backId, string name, string description,
            ICollection<CharacterBackground> characters);

        Proficiency AddProficiency(int id, ICollection<Tool> toolProficiencies, ICollection<Armor> armorProficiencies,
            ICollection<Weapon> weaponProficiencies, ICollection<Language> languages);
        
        //Combat

        CombatStats AddCombatStats(int id, int armorClass, int initiative, int speed, int? currentHp, int maxHp,
            int? tempHp, HitDie hitDie, int deathSuccess, int deathFailures, AttackSpellCasting attackSpellCasting);

        AttackSpellCasting AddAttackSpellCasting(int id, ICollection<Spell> spells, ICollection<Weapon> weapons,
            int? numberOfAttacks);

       HitDie AddHitDie(int id, int amount, int amountDice, Die die, int bonus, string type);

       //Equipment
       
        Equipment AddEquipment(int id, ICollection<Item> items, Treasure treasure);

        Item AddItem(int id, string name, int amount);

        Treasure AddTreasure(int id, ICollection<Item> items, int copperPiece, int silverPiece, int electrumPiece,
            int goldPiece, int platimumPiece);

        Weapon AddWeapon(int id, string name, int amount, HitDie hitDie, string profRequirement);
        
        //Spellcasting

        Spell AddSpell(int id, int level, string name, string description, string castingTime, string range,
            string attackSave, string type);

        Spellcast AddSpellcast(int id, string spellcastingAbility, int spellSaveDc, int spellAtkBonus,
            ICollection<SpellList> spellLists);
        
        SpellList AddSpellList(int id, bool cantrip, int level, int spellSlot, int slotExpended,
            ICollection<Spell> spells);
        
        //Stats

        Stats AddStats(int id, Attribute attribute, Skills skills, AdditionalStats additionalStats);

        AdditionalStats AddAdditionalStats(int id, ICollection<AdditionalAttribute> savingThrows, int proficiencyBonus,
            int? inspiration, int? passiveWisdom);

        Attribute AddAttribute(int id, string name, AttributeTypeEnum type, int points);

        Skills AddSkills(int id, int acrobatics, int animalHandling, int arcana, int athletics, int deception,
            int history, int insight, int intimidation, int investigation, int medicine, int nature, int perception,
            int performance, int persuasion, int religion, int sleightOfHand, int stealth, int survival);
        
        //===========================================GET===========================================
        //Main
        Character GetCharacter(int charId);

        AlliesOrganizations GetAlliesOrganizations(int id);

        Appearance GetAppearance(int id);

        Background GetBackground(int backId);

        Proficiency GetProficiency(int id);
        
        //Combat

        CombatStats GetCombatStats(int id);

        AttackSpellCasting GetAttackSpellCasting(int id);

       HitDie GetHitDie(int id);

       //Equipment
       
        Equipment GetEquipment(int id);

        Item GetItem(int id);

        Treasure GetTreasure(int id);

        Weapon GetWeapon(int id);
        
        //Spellcasting

        Spell GetSpell(int id);

        Spellcast GetSpellcast(int id);
        
        SpellList GetSpellList(int id);
        
        //Stats

        Stats GetStats(int id);

        AdditionalStats GetAdditionalStats(int id);

        Attribute GetAttribute(int id);

        Skills GetSkills(int id);
        
        //===========================================GETALL===========================================
        //Main
        
        IQueryable<Character> GetAllCharacters();
        
        IQueryable<Background> GetAllBackgrounds();
        
        IQueryable<AlliesOrganizations> GetAllAlliesOrganizations();
        
        IQueryable<CharacterBackground> GetAllCharacterBackgrounds();
        
        IQueryable<Appearance> GetAllAppearances();
        
        IQueryable<CharacterAllies> GetAllCharacterAllies();
        
        IQueryable<Proficiency> GetAllProficiencies();
        
        //COMBAT
        
        IQueryable<AttackSpellCasting> GetAllAttackSpellCastings();
        
        IQueryable<CombatStats> GetAllCombatStats();
        
        IQueryable<HitDie> GetAllHitDice();
        
        //EQUIPMENT
        
        IQueryable<Equipment> GetAllEquipments();
        
        IQueryable<Item> GetAllItems();
        
        IQueryable<Treasure> GetAllTreasures();
        
        IQueryable<Weapon> GetAllWeapons();
        
        //SPELLCASTING
        
        IQueryable<Spell> GetAllSpells();
        
        IQueryable<Spellcast> GetAllSpellcasts();
        
        IQueryable<SpellList> GetAllSpellLists();
        
        //STATS
        
        IQueryable<AdditionalStats> GetAllAdditionalStats();
        
        IQueryable<Attribute> GetAllAttributes();
        
        IQueryable<Skills> GetAllSkills();
        
        IQueryable<Stats> GetAllStats();

        //===========================================REMOVE===========================================
        //Main
        
        void RemoveCharacter(int id);
        
        void RemoveBackground(int id);
        
        void RemoveAlliesOrganizations(int id);
        
        void RemoveCharacterBackground(int charId, int backId);
        
        void RemoveCharacterAllies(int charId, int alliesId);
        
        void RemoveAppearance(int id);
        
        void RemoveProficiency(int id);
        
        //COMBAT
        
        void RemoveAttackSpellCasting(int id);
        
        void RemoveCombatStats(int id);
        
        void RemoveHitDie(int id);
        
        //EQUIPMENT
        
        void RemoveEquipment(int id);
        
        void RemoveItem(int id);
        
        void RemoveTreasure(int id);
        
        void RemoveWeapon(int id);
        
        //SPELLCASTING
        
        void RemoveSpell(int id);
        
        void RemoveSpellcast(int id);
        
        void RemoveSpellList(int id);
        
        //SKILL
        
        void RemoveAdditionalStats(int id);
        
        void RemoveAttribute(int id);
        
        void RemoveSkills(int id);
        
        void RemoveStats(int id);

        //===========================================LINK===========================================

        void LinkCharacterToBackground(int charId, int backId);

        void LinkCharacterToAllies(int charId, int alliesId);

    }
}