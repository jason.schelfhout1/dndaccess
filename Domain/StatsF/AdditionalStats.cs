﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.StatsF
{
    public class AdditionalStats : IValidatableObject
    {
        [Key]
        public int Id { get; set; }

        
        [Column(TypeName = "varchar(5)")]
        public ICollection<AdditionalAttribute> SavingThrows { get; set; }
        
        [Required]
        public int ProficiencyBonus { get; set; }

        public int? Inspiration { get; set; }

        public int? PassiveWisdom { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (ProficiencyBonus < 0 )
            {
                results.Add(new ValidationResult("PROFICIENCYBONUS CAN'T BE UNDER 0"));
            }
            
            if (Inspiration < 0 )
            {
                results.Add(new ValidationResult("INSPIRATION CAN'T BE UNDER 0"));
            }
            
            if (PassiveWisdom < 0 )
            {
                results.Add(new ValidationResult("PASSIVEWISDOM CAN'T BE UNDER 0"));
            }

            return results;
        }

    }
}