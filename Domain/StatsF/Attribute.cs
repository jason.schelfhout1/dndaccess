﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.StatsF
{
    public class Attribute : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public AttributeTypeEnum Type { get; set; }

        [Required]
        public int Points { get; set; }

        [Required]
        public int Modifier
        {
            get => Modifier;
            set
            {
                Modifier = (value - 10) / 2;
            }

        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Points < 0 )
            {
                results.Add(new ValidationResult("POINTS CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}