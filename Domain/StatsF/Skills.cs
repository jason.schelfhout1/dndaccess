﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.StatsF
{
    public class Skills : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int Acrobatics { get; set; }

        [Required]
        public int AnimalHandling { get; set; }

        [Required]
        public int Arcana { get; set; }

        [Required]
        public int Athletics { get; set; }

        [Required]
        public int Deception { get; set; }

        [Required]
        public int History { get; set; }

        [Required]
        public int Insight { get; set; }

        [Required]
        public int Intimidation { get; set; }

        [Required]
        public int Investigation { get; set; }

        [Required]
        public int Medicine { get; set; }

        [Required]
        public int Nature { get; set; }

        [Required]
        public int Perception { get; set; }

        [Required]
        public int Performance { get; set; }

        [Required]
        public int Persuasion { get; set; }

        [Required]
        public int Religion { get; set; }

        [Required]
        public int SleightOfHand { get; set; }

        [Required]
        public int Stealth { get; set; }

        [Required]
        public int Survival { get; set; }
        

        //Improve
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Acrobatics < 0 )
            {
                results.Add(new ValidationResult("ACROBATICS CAN'T BE UNDER 0"));
            }

            if (AnimalHandling < 0 )
            {
                results.Add(new ValidationResult("ANIMALHANDLING CAN'T BE UNDER 0"));
            }

            if (Arcana < 0 )
            {
                results.Add(new ValidationResult("ARCANA CAN'T BE UNDER 0"));
            }

            if (Athletics < 0 )
            {
                results.Add(new ValidationResult("ATHLETICS CAN'T BE UNDER 0"));
            }

            if (Deception < 0 )
            {
                results.Add(new ValidationResult("DECEPTION CAN'T BE UNDER 0"));
            }

            if (History < 0 )
            {
                results.Add(new ValidationResult("HISTORY CAN'T BE UNDER 0"));
            }

            if (Insight < 0 )
            {
                results.Add(new ValidationResult("INSIGHT CAN'T BE UNDER 0"));
            }

            if (Intimidation < 0 )
            {
                results.Add(new ValidationResult("INTIMIDATION CAN'T BE UNDER 0"));
            }

            if (Investigation < 0 )
            {
                results.Add(new ValidationResult("INVESTIGATION CAN'T BE UNDER 0"));
            }

            if (Medicine < 0 )
            {
                results.Add(new ValidationResult("MEDICINE CAN'T BE UNDER 0"));
            }

            if (Nature < 0 )
            {
                results.Add(new ValidationResult("NATURE CAN'T BE UNDER 0"));
            }

            if (Perception < 0 )
            {
                results.Add(new ValidationResult("PERCEPTION CAN'T BE UNDER 0"));
            }

            if (Performance < 0 )
            {
                results.Add(new ValidationResult("PERFORMANCE CAN'T BE UNDER 0"));
            }

            if (Persuasion < 0 )
            {
                results.Add(new ValidationResult("PERSUASION CAN'T BE UNDER 0"));
            }

            if (Religion < 0 )
            {
                results.Add(new ValidationResult("RELIGION CAN'T BE UNDER 0"));
            }

            if (SleightOfHand < 0 )
            {
                results.Add(new ValidationResult("SLEIGHTOFHAND CAN'T BE UNDER 0"));
            }

            if (Stealth < 0 )
            {
                results.Add(new ValidationResult("STEALTH CAN'T BE UNDER 0"));
            }

            if (Survival < 0 )
            {
                results.Add(new ValidationResult("SURVIVAL CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}