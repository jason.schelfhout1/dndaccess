﻿using System.ComponentModel.DataAnnotations;

namespace Domain.StatsF
{
    public class Stats
    {
        [Key]
        public int Id { get; set; }
        public Attribute Attribute { get; set; }
        public Skills Skills { get; set; }
        public AdditionalStats AdditionalStats { get; set; }
    }
}