﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.StatsF
{
    public class AdditionalAttribute
    {
        [Key] public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(5)")]
        public AttributeTypeEnum AttributeTypeEnum { get; set; }
        
        [Required] public AdditionalStats AdditionalStats { get; set; }
    }
}