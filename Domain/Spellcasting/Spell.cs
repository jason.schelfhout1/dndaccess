﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Spellcasting
{
    public class Spell : IValidatableObject

    {
        [Key] public int Id { get; set; }

        [Required] public int Level { get; set; }

        [Required] public string Name { get; set; }

        public string Description { get; set; }

        [Required] public string CastingTime { get; set; }

        [Required] public string Range { get; set; }

        public string AttackSave { get; set; }

        public string Type { get; set; }
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Level < 0 )
            {
                results.Add(new ValidationResult("LEVEL CAN'T BE UNDER 0"));
            }
                
            if (!CastingTime.GetType().FullName.ToLower().Contains("string") && !CastingTime.GetType().FullName.ToLower().Contains("DateTime"))
            {
                results.Add(new ValidationResult("CASTINGTIME IS NOT A STRING OR DATETIME"));
            }

            return results;
        }
    }
}