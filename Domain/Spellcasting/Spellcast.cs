﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Spellcasting
{
    public class Spellcast : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string SpellcastingAbility { get; set; }

        [Required]
        public int SpellSaveDC { get; set; }

        [Required]
        public int SpellATKBonus { get; set; }

        public ICollection<SpellList> SpellLists { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (SpellSaveDC < 0 )
            {
                results.Add(new ValidationResult("SPELLSAVEDC CAN'T BE UNDER 0"));
            }
            
            if (SpellATKBonus < 0 )
            {
                results.Add(new ValidationResult("SpellATKBonus CAN'T BE UNDER 0"));
            }
            

            return results;
        }
    }
}