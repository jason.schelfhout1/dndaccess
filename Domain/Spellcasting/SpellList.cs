﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.Spellcasting
{
    public class SpellList
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public bool Cantrip { get; set; }

        public int Level { get; set; }

        public int SpellSlot { get; set; }

        [DefaultValue(0)]
        public int SlotExpended { get; set; }

        [Required]
        public ICollection<Spell> Spells { get; set; }
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Level < 0 )
            {
                results.Add(new ValidationResult("SPELLSAVEDC CAN'T BE UNDER 0"));
            }
            
            if (SpellSlot < 0 )
            {
                results.Add(new ValidationResult("SpellATKBonus CAN'T BE UNDER 0"));
            }
            
            if (SlotExpended < 0 )
            {
                results.Add(new ValidationResult("SpellATKBonus CAN'T BE UNDER 0"));
            }
            

            return results;
        }
    }
}