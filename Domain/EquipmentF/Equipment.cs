﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.EquipmentF
{
    public class Equipment
    {
        [Key]
        public int Id { get; set; }
        
        public ICollection<Item> Items { get; set; }

        public Treasure Treasure { get; set; }
    }
}