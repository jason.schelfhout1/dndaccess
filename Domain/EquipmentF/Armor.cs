﻿using System.ComponentModel.DataAnnotations;
using Domain.Main;

namespace Domain.EquipmentF
{
    public class Armor : Item
    {
        [Key]
        public int Id { get; set; }

        public string ArmorProficiency { get; set; }

        public int ArmorClass { get; set; }

        public bool StealthDis { get; set; }

        public int MovementMod { get; set; }

        public int DexMod { get; set; }

        public int Weight { get; set; }

        public int StrengthReq { get; set; }

        public int GoldCost { get; set; }

        public Proficiency Proficiency { get; set; }
    }
}