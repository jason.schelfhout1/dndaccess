﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.EquipmentF
{
    public class Treasure
    {
        [Key]
        public int Id { get; set; }
        
        public ICollection<Item> Items { get; set; }
        
        [DefaultValue(0)]
        public int CopperPiece { get; set; }

        [DefaultValue(0)]
        public int SilverPiece { get; set; }

        [DefaultValue(0)]
        public int ElectrumPiece { get; set; }

        [DefaultValue(0)]
        public int GoldPiece { get; set; }

        [DefaultValue(0)]
        public int PlatimumPiece { get; set; }
    }
}