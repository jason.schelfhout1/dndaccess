﻿using System.ComponentModel.DataAnnotations;
using Domain.Combat;
using Domain.Main;

namespace Domain.EquipmentF
{
    public class Weapon : Item
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public HitDie HitDie { get; set; }
        
        public string ProfRequirement { get; set; }

        public Proficiency Proficiency { get; set; }
    }
}