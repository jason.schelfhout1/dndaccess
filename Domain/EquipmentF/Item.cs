﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.EquipmentF
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [DefaultValue(1)]
        public int Amount { get; set; }
    }
}