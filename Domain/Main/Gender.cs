﻿namespace Domain.Main
{
    public enum Gender
    {
        M = 1,
        F = 2,
        U = 3
    }
}