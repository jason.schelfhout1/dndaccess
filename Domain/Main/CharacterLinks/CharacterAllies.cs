﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Main.CharacterLinks
{
    //Relationship class between Character & AlliesOrganizations
    public class CharacterAllies
    {
        [Required]
        public Character Character { get; set; }

        [Required]
        public AlliesOrganizations AlliesOrganizations { get; set; }
    }
}