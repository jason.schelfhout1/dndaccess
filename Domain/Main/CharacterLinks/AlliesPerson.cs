﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Main.CharacterLinks
{
    public class AlliesPerson
    {
        [Required] 
        public AlliesOrganizations AlliesOrganizations { get; set; }
        
        [Required] 
        public Person Person { get; set; }
    }
}