﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Main.CharacterLinks
{
    //Relationship class between Character & Background
    public class CharacterBackground
    {
        [Required]
        public Character Character { get; set; }

        [Required]
        public Background Background { get; set; }
    }
}