﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.EquipmentF;

namespace Domain.Main
{
    public class Proficiency
    {
        [Key] 
        public int Id { get; set; }
        //Extend?
        [Required]
        public ICollection<Tool> ToolProficiencies { get; set; }
        
        [Required]
        public ICollection<Armor> ArmorProficiencies { get; set; }
        
        [Required]
        public ICollection<Weapon> WeaponProficiencies { get; set; }
        
        [Required]
        public ICollection<Language> Languages { get; set; }

    }
}