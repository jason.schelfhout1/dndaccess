﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Main.CharacterLinks;

namespace Domain.Main
{
    public class AlliesOrganizations
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }

        public ICollection<CharacterAllies> Characters { get; set; }
        public ICollection<AlliesPerson> Persons { get; set; }
    }
}