﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Main
{
    public class Appearance : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        
        [Required]
        public int Age { get; set; }

        
        [Required]
        public int Height { get; set; }

        
        [Required]
        public string Eyes { get; set; }

        
        [Required]
        public string Skin { get; set; }

        
        [Required]
        public string Hair { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Age <= 0 )
            {
                results.Add(new ValidationResult("AGE CAN'T BE UNDER 1"));
            }
            
            if (Height < 0 )
            {
                results.Add(new ValidationResult("Height CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}