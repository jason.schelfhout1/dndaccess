﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Combat;
using Domain.EquipmentF;
using Domain.Main.CharacterLinks;
using Domain.Spellcasting;
using Domain.StatsF;

namespace Domain.Main
{
    public class Character : IValidatableObject
    {
        [Key]
        public int CharId { get; set; }

        [Required]
        public string Name { get; set; }
        
        public string SurName { get; set; }

        [EnumDataType(typeof(Gender))]
        public Gender Gender { get; set; }
        
        public string CharClass { get; set; }

        [DefaultValue(1)]
        public int Level { get; set; }

        //Race class maken
        public string Race { get; set; }

        //Subrace class maken
        public string Subrace { get; set; }
        
        public ICollection<CharacterBackground> Backgrounds { get; set; }
        
        public Alignment Alignment { get; set; }
        
        public Appearance Appearance { get; set; }

        public string PlayerName { get; set; }

        [DefaultValue(0)]
        public int EXPPoints { get; set; }
        
        public string Backstory { get; set; }
        
        public ICollection<CharacterAllies> AlliesOrganizations { get; set; }
        
        public Proficiency Proficiency { get; set; }
        
        public Stats Stats { get; set; }
        
        public Equipment Equipment { get; set; }
        
        public Spellcast Spellcasting { get; set; }
        
        public CombatStats CombatStats { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Level <= 0 )
            {
                results.Add(new ValidationResult("LEVEL CAN'T BE UNDER 1"));
            }
            
            if (EXPPoints < 0 )
            {
                results.Add(new ValidationResult("EXP CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}