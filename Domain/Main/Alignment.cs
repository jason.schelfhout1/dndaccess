﻿namespace Domain.Main
{
    public enum Alignment
    {
        TN = 1,
        NG = 2,
        NE = 3,
        LN = 4,
        LG = 5,
        LE = 6,
        CN = 7,
        CG = 8,
        CE = 9
    }
}