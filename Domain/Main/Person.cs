﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Main.CharacterLinks;

namespace Domain.Main
{
    public class Person
    {
        [Key] public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<AlliesPerson> AlliesOrganizations { get; set; }
    }
}