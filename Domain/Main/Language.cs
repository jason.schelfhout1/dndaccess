﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Main
{
    public class Language
    {
        [Key] public int Id { get; set; }

        public string Name { get; set; }

        public Proficiency Proficiency { get; set; }
    }
}