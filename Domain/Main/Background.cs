﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Main.CharacterLinks;

namespace Domain.Main
{
    public class Background
    {
        [Key]
        public int BackId { get; set; }

        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }

        public ICollection<CharacterBackground> Characters { get; set; }
    }
}