﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Global;

namespace Domain.Combat
{
    public class HitDie : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        [DefaultValue(1)]
        public int Amount { get; set; }

        [DefaultValue(1)]
        public int AmountDice { get; set; }

        [Required]
        public Die Die { get; set; }

        public int Bonus { get; set; }

        public string Type { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (Amount < 0 )
            {
                results.Add(new ValidationResult("AMOUNT CAN'T BE UNDER 0"));
            }
            
            if (Bonus < 0 )
            {
                results.Add(new ValidationResult("BONUS CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}