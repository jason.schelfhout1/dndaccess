﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.Combat
{
    public class CombatStats : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int ArmorClass { get; set; }
        
        [Required]
        public int Initiative { get; set; }
        
        [Required]
        public int Speed { get; set; }
        
        public int? CurrentHp { get; set; }
        
        [Required]
        public int MaxHp { get; set; }
        
        public int? TempHp { get; set; }
        
        [Required]
        public HitDie HitDie { get; set; }
        
        [DefaultValue(0)]
        public int DeathSuccess { get; set; }
        
        [DefaultValue(0)]
        public int DeathFailures { get; set; }
        
        public AttackSpellCasting AttackSpellCasting { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (ArmorClass < 0)
            {
                results.Add(new ValidationResult("ARMORCLASS CAN'T BE UNDER 0"));
            }
            
            if (Initiative < 0)
            {
                results.Add(new ValidationResult("INITIATIVE CAN'T BE UNDER 0"));
            }
            
            if (MaxHp < 0)
            {
                results.Add(new ValidationResult("MAXHP CAN'T BE UNDER 0"));
            }
            
            if (DeathFailures < 0)
            {
                results.Add(new ValidationResult("DEATHFAILURES CAN'T BE UNDER 0"));
            }
            
            if (DeathSuccess < 0)
            {
                results.Add(new ValidationResult("DEATHSUCCESS CAN'T BE UNDER 0"));
            }

            return results;
        }
    }
}