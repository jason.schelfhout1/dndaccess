﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.EquipmentF;
using Domain.Spellcasting;

namespace Domain.Combat
{
    public class AttackSpellCasting : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        
        public ICollection<Spell> Spells { get; set; }
        
        public ICollection<Weapon> Weapons { get; set; }

        public int? NumberOfAttacks { get; set; }
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (NumberOfAttacks < 0 )
            {
                results.Add(new ValidationResult("NUMBEROFATTACKS CAN'T BE UNDER 1"));
            }

            return results;
        }
    }
}