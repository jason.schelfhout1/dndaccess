﻿using System;
using DAL.EF;
using DND.BL;

namespace DND.UI.CA
{
    class Program
    {
        private static readonly IManager Manager = new Manager(new Repository(new CharactersDbContext()));
        static void Main(string[] args)
        {
            Console.WriteLine("First Character: "+Manager.GetCharacter(1).Name);
        }
    }
}