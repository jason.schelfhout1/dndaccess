﻿using Microsoft.AspNetCore.Mvc;

namespace DND.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}